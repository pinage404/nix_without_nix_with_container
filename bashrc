#!/usr/bin/env bash
# hacky solution to have some packages in the `PATH`
# i don't understand how to do it with `nix profile install`
PACKAGES_PATH_FROM_FLAKE="$(nix develop /setup --command sh -c 'echo $PATH')"
export PATH="$PATH:$PACKAGES_PATH_FROM_FLAKE"

eval "$(direnv hook bash)"
