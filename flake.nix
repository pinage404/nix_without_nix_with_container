{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        devShells.default = pkgs.mkShell {
          packages = [
            # needed to copy Nix Store
            pkgs.rsync

            # basic environnement
            pkgs.bashInteractive
            pkgs.direnv

            # avoid rebuilding stuff by using cache
            pkgs.cachix

            # some editors
            pkgs.vim
            pkgs.nano
          ];
        };
      });
}
