FROM nixos/nix:2.21.0

COPY nix.conf /root/.config/nix/
COPY bashrc /root/.bashrc
COPY gitconfig /root/.gitconfig
