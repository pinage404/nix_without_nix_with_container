# Nix without Nix with Container

This is a workaround for

* [#3435](https://github.com/NixOS/nix/issues/3435)
* [#6667](https://github.com/NixOS/nix/issues/6667)
* [#9969](https://github.com/NixOS/nix/issues/9969)

## Usage

### First, copy the Nix Store to a volume

This avoid to re-download everything if the container is killed or have a problem

```sh
docker compose up copy_nix_store
```

### Second, start the Nix container

```sh
docker compose up --build --detach nix_without_nix
```

### Third, enter in the Nix container

```sh
docker compose exec nix_without_nix bash
```

## Add your working directory

Optionnaly, if you want to bind a host working directory with one in the container

In `compose.yml`, in the `nix_without_nix` service, add [a `develop` section](https://docs.docker.com/compose/compose-file/develop/)

OR

In `compose.yml`, in the `nix_without_nix` service, in the `volume` section, add :

```yaml
      - type: bind
        source: /path/to/your/project/that/use/nix
        target: /working_dir
```

Then re-run previous commands
